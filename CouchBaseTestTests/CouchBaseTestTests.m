//
//  CouchBaseTestTests.m
//  CouchBaseTestTests
//
//  Created by User on 6/22/16.
//  Copyright © 2016 User. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Document.h"

@interface CouchBaseTestTests : XCTestCase

@property(nonatomic) Document *docTestObject;

@end

@interface Document (Test)
- (id) init;
- (NSString *) createTheDocument: (NSString *) dbName DocumentData: (NSDictionary *) docData;

@end

@implementation CouchBaseTestTests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    if (self.docTestObject == nil)
        self.docTestObject = [Document sharedInstance];
    if (self.docTestObject.cblManager == nil)
        [self.docTestObject createCBLManager];
    if (self.docTestObject.cblDatabase == nil)
        [self.docTestObject createCBLDatabase: @"cbltestdb"];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void) testDocumentIdUniques
{
    NSString *firstDocId = [self.docTestObject createCBLDocument: self.docTestObject.cblDatabase];
    NSString *secDocId = [self.docTestObject createCBLDocument: self.docTestObject.cblDatabase];
    if (firstDocId == nil || secDocId == nil)
    {
        if (firstDocId == nil)
        {
            XCTAssertNotNil(firstDocId, @"The first document ID is nil");
        } else if (secDocId == nil) {
            XCTAssertNotNil(secDocId, @"The second document ID is nil");
        }
    } else {
        XCTAssertNotEqualObjects(firstDocId, secDocId, @"The first document ID is different from the second document ID");
    }
}

- (void) testDocumentRevisionID
{
    NSDictionary *jsonData = @{@"company" : @"couchbase", @"business" : @"software", @"city" : @"mountainview", @"zip" : @"94000"};
    CBLRevision *revID = [self.docTestObject createAndWriteCBLDocument: self.docTestObject.cblDatabase Document:jsonData];
    XCTAssertNotNil(revID, @"The revision ID is not nil");
    
}

- (void) testDocumentCount
{
    NSInteger curCount = [self.docTestObject getCBLDocumentCount: self.docTestObject.cblDatabase];
    NSDictionary *jsonData = @{@"company" : @"apple", @"business" : @"software", @"city" : @"mountainview", @"zip" : @"94005"};
    [self.docTestObject createAndWriteCBLDocument: self.docTestObject.cblDatabase Document:jsonData];
    NSInteger nextCount = [self.docTestObject getCBLDocumentCount: self.docTestObject.cblDatabase];
    XCTAssertEqual(curCount + 1, nextCount, @"The document count increased by 1 successfully");
    
}

- (void) testDocumentWithNilID
{
    CBLDocument *cblDoc = nil;
    NSString *docID = nil;
    cblDoc = [self.docTestObject.cblDatabase documentWithID: docID];
    
    XCTAssertNil(cblDoc, @"The document retrieved with a nil ID is nil");
}

- (void) testDocumentRetrieveWithSameID
{
    CBLDocument *cblDoc = [self.docTestObject.cblDatabase createDocument];
    NSString *docId = cblDoc.documentID;
    NSDictionary *jsonData = @{@"company" : @"google", @"business" : @"software", @"city" : @"mountainview", @"zip" : @"94005"};
    NSError *error;
    [cblDoc putProperties: jsonData error: &error];
    CBLDocument *firstDoc = [self.docTestObject.cblDatabase existingDocumentWithID: docId];
    CBLDocument *secDoc = [self.docTestObject.cblDatabase existingDocumentWithID: docId];
    if (firstDoc == nil || secDoc == nil)
    {
        if (firstDoc == nil)
        {
            XCTAssertNotNil(firstDoc, @"The first retrieved document is nil");
        } else {
            XCTAssertNotNil(secDoc, @"The second retrieved document is nil");
        }
        
    } else {
        XCTAssertEqual(firstDoc, secDoc, @"The first document is same as the second document retrieved from the database with the same document ID");
    }
}

- (void) testDocumentWithNotExistingIDApiDocWithID
{
    CBLDocument *cblDoc = nil;
    NSString *docID = @"somethingnotexist";
    cblDoc = [self.docTestObject.cblDatabase documentWithID: docID];
    
    XCTAssertEqualObjects(docID, cblDoc.documentID, @"The document retrieved with a non existing ID creats a new document with this ID");
}

- (void) testDocumentWithExistingIDApiDocWithID
{
    CBLDocument *cblDoc = [self.docTestObject.cblDatabase createDocument];
    NSString *docId = cblDoc.documentID;
    NSDictionary *jsonData = @{@"company" : @"google", @"business" : @"software", @"city" : @"mountainview", @"zip" : @"94005"};
    NSError *error;
    [cblDoc putProperties: jsonData error: &error];
    CBLDocument *retvDoc = nil;
    retvDoc = [self.docTestObject.cblDatabase documentWithID: docId];
    
    XCTAssertEqualObjects(docId, retvDoc.documentID, @"The document retrieved with an existing ID creats a new document with this ID");
}

- (void) testDocumentWithNotExistingIDApiExistingDocWithID
{
    CBLDocument *cblDoc = nil;
    NSString *docID = @"somethingnotexist";
    cblDoc = [self.docTestObject.cblDatabase existingDocumentWithID: docID];
    
    XCTAssertNil(cblDoc, @"The document retrieved with a non existing ID is nil");
}

@end
