//
//  Document.m
//  CouchBaseTest
//
//  Created by User on 6/22/16.
//  Copyright © 2016 User. All rights reserved.
//

#import "Document.h"

static Document *docObject = nil;

@implementation Document

@synthesize cblDatabase;
@synthesize cblManager;
@synthesize docID;

- (id) init {
    self = [super init];
    
    return self;
}

+ (Document *) sharedInstance
{
    @synchronized (self) {
        if (docObject == nil) {
            docObject = [[self alloc] init];
        }
    }
    
    return docObject;
}

-(BOOL) createCBLManager
{
    cblManager = [CBLManager sharedInstance];
    if (!cblManager) {
        NSLog(@"CBLManager failed to initiate the instance of the object");
        
        return NO;
    }

    return YES;
}

-(BOOL) createCBLDatabase : (NSString *) dbName
{
    NSError *error;

    if (![CBLManager isValidDatabaseName: dbName])
    {
        NSLog(@"Bad database name");
        
        return NO;
    }
    cblDatabase = [cblManager databaseNamed: dbName error: &error];
    if (!cblDatabase)
    {
        NSLog(@"Failed to creat the database. Error message: %@", error.localizedDescription);

        return NO;
    }
    
    return YES;
}

- (NSString *) createCBLDocument: (CBLDatabase *) cblDb
{
    NSString *docId = nil;
    
    CBLDocument *cblDoc = [cblDb createDocument];
    docId = cblDoc.documentID;
    
    return docId;
}

- (CBLRevision *) createAndWriteCBLDocument: (CBLDatabase *) cblDb Document: (NSDictionary *) docData
{
    CBLRevision *revId = nil;
    NSError *error;
    
    CBLDocument *cblDoc = [cblDb createDocument];
    if (docData != nil)
    {
        revId = [cblDoc putProperties : docData error: &error];
    }
    
    return revId;
}

- (NSInteger) getCBLDocumentCount: (CBLDatabase *) cblDb
{
    NSInteger count = 0;
    count = [cblDb documentCount];
    
    return count;
}


@end
