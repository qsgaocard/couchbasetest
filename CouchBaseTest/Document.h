//
//  Document.h
//  CouchBaseTest
//
//  Created by User on 6/22/16.
//  Copyright © 2016 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CouchbaseLite/CouchbaseLite.h>
#import <CouchbaseLite/CBLDocument.h>

@interface Document : NSObject
{
    
}

@property (nonatomic, strong) CBLDatabase *cblDatabase;
@property (nonatomic, strong) CBLManager *cblManager;
@property (nonatomic, strong) NSString *docID;

+ (Document *) sharedInstance;
- (id) init;
- (BOOL) createCBLManager;
- (BOOL) createCBLDatabase : (NSString *) dbName;
- (NSString *) createCBLDocument: (CBLDatabase *) cblDB;
- (CBLRevision *) createAndWriteCBLDocument: (CBLDatabase *) cblDb Document: (NSDictionary *) docData;
- (NSInteger) getCBLDocumentCount: (CBLDatabase *) cblDb;

@end
